# EasyTools.ImgCompression

#### 介绍
- 支持将其它图片以 jpg 格式压缩或缩放
- 支持 png 格式压缩或缩放

- Support Convert Other Image to JPG and format compression or scaling
- Support PNG format compression or scaling

#### 安装教程 / Nuget
```cmd
Install-Package EasyTools.ImgCompression -Version 1.1.1
```

#### 使用说明 / Quick start
```csharp
// quality value 0-100, higher is better
ImageCompression.JpgCompression(imgStream, width = 0, height = 0, quality = 70L)

// alphaFader value 1-300, lower is better
ImageCompression.PngCompression(imgStream, width = 0, height = 0, alphaFader = 300)
```
