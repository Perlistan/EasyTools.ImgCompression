﻿using nQuant;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace EasyTools.ImgCompression
{
    public static class ImageCompression
    {
        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }

            return null;
        }

        /// <summary>
        ///  jpg 图片压缩或缩放
        /// </summary>
        /// <param name="imgStream">原图片流</param>
        /// <param name="width">（可选）宽度</param>
        /// <param name="height">（可选）高度</param>
        /// <param name="quality">缩放值 0 - 100，越低压缩程度越好</param>
        /// <returns></returns>
        public static byte[] JpgCompression(Stream imgStream, int width = 0, int height = 0, long quality = 70L)
        {
            var sourceImg = new Bitmap(imgStream);
            Bitmap newImg = null;
            try
            {
                using (var myEncoderParameters = new EncoderParameters(1))
                using (var myEncoderParameter = new EncoderParameter(Encoder.Quality, quality))
                using (var memoryStream = new MemoryStream())
                {
                    // 判断是否缩放
                    if (width + height > 0)
                    {
                        newImg = new Bitmap(sourceImg, width, height);
                        sourceImg.Dispose();
                        sourceImg = newImg;
                    }

                    // 将jpg以外的图片先转换为JPG并压缩
                    var jpgCodecInfo = GetEncoder(ImageFormat.Jpeg);
                    myEncoderParameters.Param[0] = myEncoderParameter;
                    sourceImg.Save(memoryStream, jpgCodecInfo, myEncoderParameters);
                    return memoryStream.ToArray();
                }
            }
            finally
            {
                sourceImg.Dispose();
                newImg?.Dispose();
            }
        }

        /// <summary>
        /// png 图片压缩或缩放
        /// </summary>
        /// <param name="imgStream">原图片流</param>
        /// <param name="width">（可选）宽度</param>
        /// <param name="height">（可选）高度</param>
        /// <param name="alphaFader">缩放值 1 - 300，越高压缩程度越好</param>
        /// <returns></returns>
        public static byte[] PngCompression(Stream imgStream, int width = 0, int height = 0, int alphaFader = 300)
        {
            var wuQuantizer = new WuQuantizer();
            var sourceImg = new Bitmap(imgStream);
            Bitmap newImg32Bit = null;
            Bitmap newImg = null;
            try
            {
                // 如果不是32位png则转换为32位
                var imgPix = Image.GetPixelFormatSize(sourceImg.PixelFormat);
                if (imgPix != 32)
                {
                    newImg32Bit = new Bitmap(sourceImg.Width, sourceImg.Height, PixelFormat.Format32bppArgb);
                    using (var graphics = Graphics.FromImage(newImg32Bit))
                    {
                        graphics.DrawImage(sourceImg, 0, 0);
                    }
                    sourceImg.Dispose();
                    sourceImg = newImg32Bit;
                }

                // 判断是否缩放
                if (width + height > 0)
                {
                    newImg = new Bitmap(sourceImg, width, height);
                    sourceImg.Dispose();
                    sourceImg = newImg;
                }

                using (var quantized = wuQuantizer.QuantizeImage(sourceImg, 0, alphaFader))
                using (var memoryStream = new MemoryStream())
                {
                    quantized.Save(memoryStream, ImageFormat.Png);
                    return memoryStream.ToArray();
                }
            }
            finally
            {
                newImg32Bit?.Dispose();
                newImg?.Dispose();
                sourceImg.Dispose();
            }
        }

    }
}
